# -*- coding: utf-8 -*-
# @Time    : 2020/6/29
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : URLS.py

from django.urls import path
from .views import eventviews, phishingviews, projectsviews
from Workflow.views import workflowviews, logviews

urlpatterns = [
    path('project/list/', projectsviews.mainlist, name='main_list'),
    path('project/list/levellist/', projectsviews.level_list, name='level_list'),
    path('project/create/project/', projectsviews.project_create, name='project_create'),
    path('project/update/project/<str:project_id>/', projectsviews.project_update, name='project_update'),
    path('project/delete/project/<str:project_id>/', projectsviews.project_delete, name='project_delete'),
    path('project/create/action/<str:project_id>/', projectsviews.project_action_create, name='project_action_create'),

    path('phishing/list/', phishingviews.mainlist, name='main_list'),
    path('phishing/create/phishing/', phishingviews.phishing_create, name='phishing_create'),
    path('phishing/update/phishing/<str:phishing_id>/', phishingviews.phishing_update, name='phishing_update'),
    path('phishing/delete/phishing/<str:phishing_id>/', phishingviews.phishing_delete, name='phishing_delete'),
    path('phishing/list/victim/<str:phishing_id>/', phishingviews.victimlist, name='victim_list'),


    path('event/list/', eventviews.mainlist, name='main_list'),
    path('event/list/levellist/', eventviews.level_list, name='level_list'),
    path('event/create/event/', eventviews.event_create, name='event_create'),
    path('event/update/event/<str:event_id>/', eventviews.event_update, name='event_update'),
    path('event/delete/event/<str:event_id>/', eventviews.event_delete, name='event_delete'),
    path('event/create/action/<str:event_id>/', eventviews.event_action_create, name='event_action_create'),

    path('ticket/list/<str:workflow_key>/', workflowviews.main_list),
    path('ticket/type/<str:ticket_key>/', workflowviews.ticket_list),
    path('ticket/action/<str:action>/<str:ticket_id>/', workflowviews.ticket_action),
    path('ticket/manyaction/<str:action>/', workflowviews.ticket_many_deny),
    path('ticket/details/<str:ticket_id>/', workflowviews.ticket_details),

    path('log/list/<str:ticket_id>/', logviews.ticket_checklog),

]