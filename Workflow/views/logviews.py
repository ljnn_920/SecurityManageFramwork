# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : logviews.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter
from .. import serializers
from ..Functions import ticketfun


@api_view(['GET'])
def ticket_checklog(request, ticket_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    if ticket_id is None:
        data['msg'] = '流程不存在'
    else:
        item_get = ticketfun.check_user_permission(user, ticket_id)
        if item_get:
            list_get = item_get.ticket_for_workflowLog.all().order_by('-updatetime')
            list_count = list_get.count()
            serializers_get = serializers.WorkflowLogSerializer(instance=list_get, many=True)
            data['code'] = 0
            data['msg'] = 'success'
            data['count'] = list_count
            data['data'] = xssfilter(serializers_get.data)
        else:
            data['msg'] = '你要干啥'
    return JsonResponse(data)