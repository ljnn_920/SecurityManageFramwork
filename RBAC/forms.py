# coding:utf-8
"""
Created on 2018年10月29日

@author:
"""

from django import forms
from captcha.fields import CaptchaField


class LoginForm(forms.Form):
    username = forms.CharField(label='账号', max_length=75)
    password = forms.CharField(label='密码', max_length=25)
    captcha = CaptchaField(label='验证码')


class ResetpsdForm(forms.Form):
    oldpassword = forms.CharField(label='原始密码',max_length=25)
    newpassword = forms.CharField(label='新密码',max_length=25)
    repassword = forms.CharField(label='重复密码',max_length=25)


class ProfileUpdateForm(forms.Form):
    title = forms.CharField(label='企业名称', max_length=75)
    manage = forms.CharField(label='负责人', max_length=75)
    phone = forms.CharField(label='联系电话', max_length=25)
    mail = forms.CharField(label='联系邮箱', max_length=25)

