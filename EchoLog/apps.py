from django.apps import AppConfig


class EchologConfig(AppConfig):
    name = 'EchoLog'
