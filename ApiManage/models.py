# coding:utf-8
from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Type(models.Model):
    name = models.CharField('名称', max_length=20)
    key = models.CharField('键值', max_length=20)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = '键值分类'


class SyncKey(models.Model):
    key = models.CharField('授权key', max_length=100)
    type = models.ForeignKey(Type, verbose_name='键值分类', related_name='type_for_synckey', on_delete=models.CASCADE)
    description = models.TextField('备注')
    is_active = models.BooleanField('是否有效', default=True)

    create_time = models.DateTimeField('添加时间', auto_now_add=True)
    update_time = models.DateTimeField('更新时间', auto_now=True)
    user = models.ForeignKey(User, verbose_name='创建人员', related_name='user_for_synckey', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'SyncKey'
        verbose_name_plural = '同步键值'
