# -*- coding: utf-8 -*-
# @Time    : 2020/7/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : scannerviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_protect
from .. import forms, models
from ..Functions.base import xrayfun


@api_view(['POST'])
@csrf_protect
def scanner_create(request):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    form = forms.ScannerEditForm(request.POST)
    if form.is_valid():
        form.save()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def scanner_update(request, scanner_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    scanner_item = models.Scanner.objects.filter(id=scanner_id).first()
    if scanner_item:
        form = forms.ScannerEditForm(request.POST, instance=scanner_item)
        if form.is_valid():
            form.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)


@api_view(['GET'])
def scanner_delete(request, scanner_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    scanner_item = models.Scanner.objects.filter(id=scanner_id).first()
    if scanner_item:
        scanner_item.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)


@api_view(['GET'])
def scanner_sync(request, scanner_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    scanner_item = models.Scanner.objects.filter(id=scanner_id).first()
    if scanner_item:
        if scanner_item.type == 'X-Ray':
            try:
                res = xrayfun.get_scanner_policies(scanner_item)
                if res:
                    data['code'] = 0
                    data['msg'] = 'success'
                else:
                    data['msg'] = '请检查扫描器设置'
            except:
                data['msg'] = '请检查扫描器设置'
        else:
            data['code'] = 0
            data['msg'] = '该扫描器需自行创建策略'
    else:
        data['msg'] = '请检查参数'
    return JsonResponse(data)
