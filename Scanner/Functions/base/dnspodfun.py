# -*- coding: utf-8 -*-
# @Time    : 2020/12/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : dnspodfun.py
from Asset.Functions import subdomain
import urllib3
import json
import requests


class DNSpod_API:
    def __init__(self, scanner_obj):
        self.api_url = scanner_obj.url
        self.app_id = scanner_obj.apikey
        self.app_token = scanner_obj.apisec
        self.headers = {
            'Content-Type': 'application/json',
        }

    def post_run(self, path, data_get):
        data_get['login_token'] = self.app_id + ',' + self.app_token
        data_get['format'] = 'json'
        data_get['error_on_empty'] = 'no'
        path = self.api_url + path
        urllib3.disable_warnings()
        r = requests.post(path, data=data_get)
        data_get = json.loads(r.text)
        return data_get

    def user_info(self):
        path = '/User.Detail'
        data_get = {}
        res = self.post_run(path, data_get)
        if res.get('status').get('code') == '1':
            return True
        return False

    def domain_list(self):
        domain_list = []
        path = '/Domain.List'
        data_get = {}
        res = self.post_run(path, data_get)
        for item in res.get('domains'):
            domain_list.append(item.get('name'))
        return domain_list

    def subdomain_list(self, domain):
        sub_domain_list = []
        path = '/Record.List'
        data_get = {'domain': domain, 'record_type': 'A'}
        res = self.post_run(path, data_get)
        for item in res.get('records'):
            sub_domain_list.append(item.get('name'))
        return sub_domain_list

    def outip_list(self, domain):
        out_ip_list = []
        path = '/Record.List'
        data_get = {'domain': domain, 'record_type': 'A'}
        res = self.post_run(path, data_get)
        for item in res.get('records'):
            out_ip_list.append(item.get('value'))
        return out_ip_list


def get_dnspod_res(scantask_obj):
    scanner = DNSpod_API(scantask_obj.police.scanner)
    flag = scanner.user_info()
    if flag:
        subdomain_list = []
        outip_list = []
        domain_list = scanner.domain_list()
        for domain in domain_list:
            subdomain_list = subdomain_list + scanner.subdomain_list(domain)
        subdomain.subdomain_to_asset(subdomain_list)
        for domain in domain_list:
            outip_list = subdomain_list + scanner.outip_list(domain)
        subdomain.subdomain_to_asset(outip_list)
        return True
    return False
