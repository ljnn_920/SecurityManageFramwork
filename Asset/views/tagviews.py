# -*- coding: utf-8 -*-
# @Time    : 2020/11/10
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : tagviews.py


from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_protect
from .. import forms
from ..Functions import assetfun
from django.http import JsonResponse


@api_view(['POST'])
@csrf_protect
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    form = forms.TagForm(request.POST)
    if form.is_valid():
        form.save()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)