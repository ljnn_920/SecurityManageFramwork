# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : baseinfoviews.py
# 这里用来存放资产非独立信息的函数

from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_protect
from .. import models, serializers, forms
from ..Functions import assetfun
from django.http import JsonResponse


@api_view(['POST'])
@csrf_protect
def update_views(request, key, asset_id):
    data = {
        "code": 1,
        "msg": "",
    }
    item_get = assetfun.check_asset_permission(asset_id, request.user)
    if item_get:
        if key == 'os':
            form = forms.OsForm(request.POST, instance=item_get.os_for_asset)
            if form.is_valid():
                form.save()
                data['code'] = 0
                data['msg'] = 'success'
            else:
                data['msg'] = '请检查输入'
        elif key == 'web':
            form = forms.WebForm(request.POST, instance=item_get.web_for_asset)
            if form.is_valid():
                form.save()
                data['code'] = 0
                data['msg'] = 'success'
            else:
                data['msg'] = '请检查输入'
        else:
            data['msg'] = '乱改参数是不对滴'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)



