# -*- coding: utf-8 -*-
# @Time    : 2020/6/1
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : portscan.py

from .. import models


def port_to_asset(asset_port):
    asset_get = models.Asset.objects.filter(key=asset_port.get('asset')).first()
    if asset_get:
        for item in asset_port.get('port_list'):
            port_get = models.PortInfo.objects.get_or_create(
                port=item.get('port'),
                asset=asset_get,
            )
            port_get = port_get[0]
            port_get.name = item.get('name')
            port_get.product = item.get('product')
            port_get.version = item.get('version')
            port_get.protocol = item.get('protocol')
            port_get.save()
        return True
    else:
        return False
