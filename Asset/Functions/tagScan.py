# -*- coding: utf-8 -*-
# @Time    : 2020/6/1
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : tagScan.py

from .. import models


def tag_to_asset(tag_info):
    asset_get = models.Asset.objects.filter(key=tag_info.get('asset')).first()
    if asset_get:
        tag_get = models.TagInfo.objects.get_or_create(
            name=tag_info.get('tag'),
        )
        tag_get = tag_get[0]
        tag_get.asset.add(asset_get)
        return True
    return False
