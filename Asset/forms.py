# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py
from django.forms import ModelForm
from . import models
from django import forms


class AssetForm(ModelForm):
    parent = forms.CharField(required=False)

    class Meta:
        model = models.Asset
        fields = ('name', 'key', 'type', 'description', 'is_out', 'manage', 'parent')


class AssetUpdateForm(ModelForm):
    parent = forms.CharField(required=False)

    class Meta:
        model = models.Asset
        fields = ('name', 'type', 'description', 'is_out', 'manage', 'parent')


class GroupForm(ModelForm):
    asset = forms.CharField(required=False)

    class Meta:
        model = models.Group
        fields = ('name', 'description', 'manage', 'asset', 'parent')


class PortForm(ModelForm):
    class Meta:
        model = models.PortInfo
        fields = ('port', 'product', 'version', 'port_info')


class PortUpdateForm(ModelForm):
    class Meta:
        model = models.PortInfo
        fields = ('product', 'version', 'port_info')


class PluginForm(ModelForm):
    class Meta:
        model = models.PluginInfo
        fields = ('name', 'version', 'plugin_info')


class OsForm(ModelForm):
    class Meta:
        model = models.OsInfo
        fields = ('key', 'hostname', 'os')


class WebForm(ModelForm):
    class Meta:
        model = models.WebInfo
        fields = ('key', 'middleware', 'middleware_version', 'language', 'web_framwork', 'web_framwork_version')


class TagForm(ModelForm):
    class Meta:
        model = models.TagInfo
        fields = ('name', 'description')

