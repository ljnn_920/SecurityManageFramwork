# coding:utf-8
from django.contrib import admin
from . import models

# Register your models here.


admin.site.register(models.TypeInfo)
admin.site.register(models.Type)
admin.site.register(models.Asset)
admin.site.register(models.OsInfo)
admin.site.register(models.WebInfo)
admin.site.register(models.PortInfo)
admin.site.register(models.PluginInfo)
