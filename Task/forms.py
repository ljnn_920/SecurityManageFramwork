# -*- coding: utf-8 -*-
# @Time    : 2020/6/3
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py
from django.forms import ModelForm
from . import models
from django import forms


class TaskEditForm(ModelForm):
    asset = forms.CharField(required=False)
    manage = forms.CharField(required=False)
    cycle_hours = forms.IntegerField(required=False)

    class Meta:
        model = models.Task
        fields = ('name', 'target', 'type', 'predefine', 'person', 'manage', 'predefine', 'description', 'start_time', 'cycle_hours', 'end_time', 'asset')


class PredefinForm(ModelForm):
    police = forms.CharField()

    class Meta:
        model = models.PreDefine
        fields = ('name', 'tasktype', 'key', 'police')
