# -*- coding: utf-8 -*-
# @Time    : 2020/6/3
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : codefun.py

import datetime
from .. import models


def check_apicode(apicode):
    time_now = datetime.datetime.now()
    apicode_item = models.ApiCode.objects.filter(code=apicode).first()
    if apicode_item and apicode_item.deny_time >= time_now:
        return apicode_item.task
    else:
        return None
