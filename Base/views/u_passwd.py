# -*- coding: utf-8 -*-
# @Time    : 2020/12/6
# @Author  : canyuan
# @Email   : gy071089@outlook.com


from django.http import JsonResponse
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms, serializers
import json


@api_view(['POST'])
@permission_classes((AllowAny,))
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    form = forms.U2p_Form(request.POST)
    if form.is_valid():
        username_get = form.cleaned_data['username']
        password_get = form.cleaned_data['password']
    else:
        data_get = json.loads(request.body)
        username_get = data_get.get('username')
        password_get = data_get.get('password')
    username_item = models.Username.objects.get_or_create(name=username_get)[0]
    username_item.count = username_item.count + 1
    username_item.save()
    password_item = models.U_Passwd.objects.get_or_create(name=password_get)[0]
    password_item.username.add(username_item)
    password_item.count = password_item.count + 1
    password_item.save()
    data['msg'] = '账号密码错误'
    return JsonResponse(data)


@api_view(['GET'])
def username_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        list_get = models.Username.objects.filter(name__icontains=key).order_by('count')
        list_count = list_get.count()
        serializers_get = serializers.UsernameSerializer(instance=list_get, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['GET'])
def u_passwd_list(request, username_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        if username_id == '0':
            list_get = models.U_Passwd.objects.filter(name__icontains=key).order_by('count')
        else:
            username_item = models.Username.objects.filter(id=username_id)
            list_get = models.U_Passwd.objects.filter(name__icontains=key, username=username_item).order_by('count')
        list_count = list_get.count()
        serializers_get = serializers.U_passwdSerializer(instance=list_get, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)